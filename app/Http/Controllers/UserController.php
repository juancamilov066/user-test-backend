<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'Tu madre';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::where('email', $request->post('email'))->first();
        if ($user !== null) {
            return response()->json([
                'success' => true,
                'message' => 'User already exists'
            ], 409);
        }

        $user = new User;
        $user->name = $request->post('name');
        $user->phone = $request->post('phone');
        $user->email = $request->post('email');
        $user->password = hash('sha256', $request->post('password'));

        $user->save();

        return response()->json([
            'success' => true,
            'data' => $user,
            'message' => 'User created successfully'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($email)
    {
        $user = User::where('email', $email)->first();
        if ($user !== null){
            return response()->json([
                'success' => true,
                'data' => $user,
                'message' => 'User queried successfully'
            ], 200);
        }
        return response()->json([
                'success' => true,
                'message' => 'User not found'
            ], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $email)
    {

        $user = User::where('email', $email)->first();
        if ($user !== null){
            if ($email != $request->email) {
                $existingUser = User::where('email', $request->all('email'))->first();
                if ($existingUser !== null) {
                    return response()->json([
                        'success' => true,
                        'message' => 'User already exists'
                    ], 409);
                }
            }

            $user->name = $request->name;
            $user->phone = $request->phone;
            $user->email = $request->email;

            $user->save();

            return response()->json([
                'success' => true,
                'data' => $user,
                'message' => 'User updated successfully'
            ], 200);
        }

        return response()->json([
                'success' => false,
                'message' => 'User not found'
            ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($email)
    {
        $user = User::where('email', $email)->first();
        if ($user !== null){
            $user->delete();
            return response()->json(
                [
                    'success' => true,
                    'message' => 'User deleted successfully'
                ]
                , 200);
        }
        return response()->json([
                'success' => false,
                'message' => 'User not found'
            ], 404);
    }
}
