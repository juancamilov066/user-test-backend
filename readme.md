# Users Backend End - Prueba para Intergrupo e Ilumno

Hola, bienvenido a la prueba.
Este repositorio contiene el proyecto back end de la prueba

## Instrucciones para ejecutar

Debes clonar el proyecto en un servidor que tenga MySQL y pueda ejecutar PHP (Ej: Apache).
Esta es la configuracion por defecto que esta en el archivo .env
En la raiz del proyecto encontraras el archivo `export.sql` con todos los datos necesarios
Tambien hay una carpeta llamada `database-model` donde se encuentra el modelo relacional

Configuracion  | Valor
------------- | -------------
DB_CONNECTION | mysql
DB_HOST | 127.0.0.1
DB_PORT | 3306
DB_DATABASE | users-test-db
DB_USERNAME | root
DB_PASSWORD | root

Lo puedes modificar en el archivo .env

## Informacion Extra

* Version de laravel: 5.6.26